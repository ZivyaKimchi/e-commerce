import { NgModule, ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";

const appRoutes: Routes = [
    {path:"login", component:LoginComponent},
    { path: "",redirectTo: "/login", pathMatch: "full"},
];

const appRouter: ModuleWithProviders = RouterModule.forRoot(appRoutes);

@NgModule({
    imports: [appRouter],
    exports: []
})
export class AppRoutingModule {

}