import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { User } from "../shared/models/user.model";
import { UserService } from "../shared/services/user.service";
import { HttpErrorResponse } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import swal from 'sweetalert';
import { Observable } from "rxjs/Rx";

@Component({
    selector: 'sd-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    @ViewChild('close') closeBtn: ElementRef;
    @ViewChild('loginBtn') loginBtn: ElementRef;
    loginForm: FormGroup;
    constructor(private formBuilder: FormBuilder, private userService: UserService, private route: ActivatedRoute, private router: Router) {
    }
    private createForm(): void {
        this.loginForm = this.formBuilder.group({
            userNameOrEmailControl: [""],
            userPasswordControl: [""]
        })
    }

    login(isUser: boolean) {
        if (isUser == true) {
            // sessionStorage.setItem("Admin", "true");
            this.closeBtn.nativeElement.click();
            // this.router.navigateByUrl('/home');
            swal({
                title: "",
                text: "Success Login!!",
                icon: "success"
            });
        }
        else {
            swal({
                title: "שגיאה",
                text: "שם או סיסמא שגויים",
                icon: "error"
            });
        }
    }
    logout() {
        if (sessionStorage.getItem("Admin") != null) {
            sessionStorage.removeItem("Admin");
            this.router.navigateByUrl('/home');
        }
        else
            swal({
                title: "שגיאה",
                text: "אתה מנותק,אין צורך להתנתק שוב!",
                icon: "error"
            }).then(() => { this.router.navigateByUrl("/home") });
    }

    checkLogin(): void {
        let user = new User(undefined, undefined);
        user.nameOrEmail = this.loginForm.get("userNameOrEmailControl").value;
        user.password = this.loginForm.get("userPasswordControl").value;
        this.userService.isUser(user)
            .subscribe((result: boolean) => {
                this.login(result);
            },
            (error: HttpErrorResponse) => {
                let message: string = "";
                for (let i = 0; i < error.error.length; i++) {
                    message += error.error[i] + "\n";
                }
                swal("שגיאה", message, "error");
            });
    }
    cancel() {
        this.closeBtn.nativeElement.click();
        // this.router.navigateByUrl('/home');
    }
    get isAdmin(): any {
        return sessionStorage.getItem('Admin');
    }
    ngOnInit(): void {
                this.createForm();
                this.loginBtn.nativeElement.click();
    }
}



